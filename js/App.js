﻿var imdb = angular.module('imdb', []);

imdb.controller('dannieController', function ($scope, $http) {
    $scope.movie_name = '';
    $scope.search_type = '';
    $scope.sort_type = 'Title';
    $scope.dannie = {};
    $scope.details = [];
    $scope.ID = '';

    $scope.movieDannie = function () {
        $scope.details = [];
        $http.get("http://www.omdbapi.com/?apikey=8559af41&s="+$scope.movie_name+"&type="+$scope.search_type+"&y="+$scope.movie_year+"&r=json")
        .then(function (response) {
            $scope.dannie = response.data;

            for (var i = 0; i < $scope.dannie.Search.length; i++) {
                $http.get("http://www.omdbapi.com/?apikey=8559af41&i=" + $scope.dannie.Search[i].imdbID + "&plot=full&r=json")
                    .then(function (response) { $scope.details.push(response.data); });

            }
        });
    }
});